<?php

require_once 'model/manager.php';


class Notif extends Manager{

    public $compteUser = '34000_EMITY34';

    function loadNotifs($compteUser) {
        
        $bdd = $this->dbConnect();

        $requete = $bdd->prepare("SET lc_time_names = ?;");
        $requete->execute(array('fr_FR'));

        $requete = $bdd->prepare("SELECT DATE_FORMAT(date_envoi_alerte_player, '%W %e %M %T') AS Date, nom_player AS Player, nom_task AS Description FROM players__tasks
        JOIN players ON players__tasks.id_player = players.id_player
        JOIN tasks ON players__tasks.id_task = tasks.id_task
        JOIN type_notif__players ON type_notif__players.id_player = players.id_player
        JOIN utilisateurs ON players.id_utilisateur = utilisateurs.id_utilisateur
        WHERE compte_utilisateur = ?
        ORDER BY Date DESC
        LIMIT 3");

        $requete->execute(array($compteUser));

        return $requete;
    }

    function getPlayers($compteUser) {
        
        $bdd = $this->dbConnect();

        $requete = $bdd->prepare("SET lc_time_names = ?;");
        $requete->execute(array('fr_FR'));

        $requete = $bdd->prepare("SELECT DATE_FORMAT(date_envoi_alerte_player, '%W %e %M %T') AS Date, nom_player AS Player, nom_task AS Description FROM players__tasks
        JOIN players ON players__tasks.id_player = players.id_player
        JOIN tasks ON players__tasks.id_task = tasks.id_task
        JOIN type_notif__players ON type_notif__players.id_player = players.id_player
        JOIN utilisateurs ON players.id_utilisateur = utilisateurs.id_utilisateur
        WHERE compte_utilisateur = ?
        AND type_notif__players.id_type_notif = 2
        ORDER BY Date DESC");

        $requete->execute(array($compteUser));

        return $requete;
    }

    function criticalAlert($compteUser) {
        
        $bdd = $this->dbConnect();

        $requete = $bdd->prepare("SELECT players__tasks.id_task, nom_player, players__tasks.id_player FROM players__tasks
        JOIN players ON players__tasks.id_player = players.id_player
        JOIN tasks ON players__tasks.id_task = tasks.id_task
        JOIN type_notif__players ON type_notif__players.id_player = players.id_player
        JOIN utilisateurs ON players.id_utilisateur = utilisateurs.id_utilisateur
        WHERE compte_utilisateur = ?
        AND type_notif__players.id_type_notif = 2
        AND players__tasks.id_task = 2
        AND bool_old_player = 0");

        $requete->execute(array($compteUser));

        return $requete;
    }

}

?>