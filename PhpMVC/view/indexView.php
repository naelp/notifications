<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="public/css/bootstrap.min.css">
        <link rel="stylesheet" href="public/css/alertify.min.css">
        <link rel="stylesheet" href="public/css/emity.css">
        <link rel="stylesheet" href="public/css/main.css">
        <title>PopUp</title>
    </head>

    <body>
        <h1 class="text-center"> EMITY ACCUEIL </h1>
        <div class="dropdown">
            <button class="btn btn-sm btn-primary dropdown-toggle ml-3" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Mes notifications
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <?php
                    while($donnees = $requete->fetch()) {
                        echo "<a class='dropdown-item' href='index.php?page=notif'> "
                        . ucfirst($donnees['Date']) . ' | ' . $donnees['Player'] . ' | ' . $donnees['Description'] .
                        "</a>";
                    }

                    $requete->closeCursor();

                ?>

            </div>
        </div>
    </body>
    <script type='text/javascript' src='public/js/jquery.js'></script>
    <script type='text/javascript' src='public/js/popper.js'></script>
    <script type='text/javascript' src='public/js/bootstrap.min.js'></script>
    <script type='text/javascript' src='public/js/main.js'></script>
    <script type='text/javascript' src='public/js/alertify.min.js'></script>

</html>