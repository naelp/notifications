<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Notifications</title>
        <link rel="stylesheet" type="text/css" href="public/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="public/css/dataTables.bootstrap4.min.css" />
        <link rel="stylesheet" type="text/css" href="public/css/dataTables.min.css"/>
    </head>
    
    <body>
        <section>
            <h1 class="text-center"> NOTIFICATIONS </h1>

            <div id="datepicker" class="mb-5"></div>

            <div class="container mt-5 mb-5">

                <table id="playlist-table" class="display table table-hover" style="width:100%">
                    <thead id="tabHead">

                    </thead>
                    
                    <tbody id="tabLog">

                    </tbody>

                    <tfoot id="tabFoot">
                        
                    </tfoot>
                </table>
            </div>
        </section>
    </body>
    
    <script type="text/javascript" src="public/js/jquery.js"></script>
    <script type="text/javascript" src="public/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="public/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="public/js/main.js"></script>
    <script type="text/javascript" src="public/js/notif.js"></script>
    <script type="text/javascript" src="public/js/dataTables.min.js"></script>

</html>