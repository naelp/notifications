<?php

require 'model/notif.php';

function getNotif() {

    $notif = new Notif();
    $requete = $notif->loadNotifs($notif->compteUser);
    require 'view/indexView.php';
}

function dataNotif() {

    require 'view/notifView.php';

}