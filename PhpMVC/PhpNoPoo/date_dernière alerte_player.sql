SELECT HOUR(TIME(NOW())) AS Heure, players.id_player, id_task, compte_utilisateur, date_envoi_alerte_player, nom_player FROM players__tasks
JOIN players ON players__tasks.id_player = players.id_player
JOIN type_notif__players ON type_notif__players.id_player = players.id_player
JOIN utilisateurs ON players.id_utilisateur = utilisateurs.id_utilisateur
WHERE compte_utilisateur = ? 
AND TIMEDIFF(NOW(), date_envoi_alerte_player) > '01:00:00'
AND type_notif__players.id_type_notif = 2
AND bool_old_player = 0