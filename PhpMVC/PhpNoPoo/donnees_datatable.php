<?php

require 'manager.php';

$compteUtilisateur = '34000_EMITY34';
$bdd = Manager::ConnexionBDD();
$message = [];
$j = 0;

$requete = $bdd->prepare("SET lc_time_names = ?;");
$requete->execute(array('fr_FR'));

$requete = $bdd->prepare("SELECT DATE_FORMAT(date_envoi_alerte_player, '%W %e %M %T') AS Date, nom_player AS Player, nom_task AS Description FROM players__tasks
JOIN players ON players__tasks.id_player = players.id_player
JOIN tasks ON players__tasks.id_task = tasks.id_task
JOIN type_notif__players ON type_notif__players.id_player = players.id_player
JOIN utilisateurs ON players.id_utilisateur = utilisateurs.id_utilisateur
WHERE compte_utilisateur = ?
AND type_notif__players.id_type_notif = 2
ORDER BY Date DESC");

$requete->execute(array($compteUtilisateur));

while ($donneesPlayer = $requete->fetch()) {
    $message['Player'][$j] = $donneesPlayer['Player'];
    $message['Description'][$j] = $donneesPlayer['Description'];
    $message['Date'][$j] = ucfirst($donneesPlayer['Date']);
    $j++;
}

echo json_encode($message);

?>