<!DOCTYPE html>
<html lang="fr">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/alertify.min.css">
        <link rel="stylesheet" href="css/emity.css">
        <link rel="stylesheet" href="css/main.css">
        <title>PopUp</title>
    </head>

    <body>
        <h1 class="text-center"> EMITY ACCUEIL </h1>
        <div class="dropdown">
            <button class="btn btn-sm btn-primary dropdown-toggle ml-3" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Mes notifications
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            </div>
        </div>
    </body>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/popper.js'></script>
    <script type='text/javascript' src='js/bootstrap.min.js'></script>
    <script type='text/javascript' src='js/main.js'></script>
    <script type='text/javascript' src='js/alertify.min.js'></script>

</html>