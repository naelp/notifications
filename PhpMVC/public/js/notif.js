var table;
var tab;
var color;
var dateLog;
var alert;
var tabH;
var lengthPreviousArray;

const configuration = {
    responsive: true, 
    ordering: "isSorted", 
    order: [],
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "Tous"]], 
    language: {
        info: "Informations _START_ à _END_ sur _TOTAL_ au total",
        emptyTable: "Aucune information utilisateur",
        lengthMenu: "_MENU_ résultats par page",
        search: "Rechercher : ",
        zeroRecords: "Aucun résultat de recherche",
        paginate: {
            previous: "Précédent",
            next: "Suivant"
        },        
        sInfoFiltered:   "<br/>(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
        sInfoEmpty:      "résultat 0 à 0 sur 0 au total <br/>",
    },
    columns: [

    ]
}

$(document).ready(function() {
    $.ajax({
        type:"POST",
        url: "public/phpRequests/donnees_datatable.php",
        dataType: 'json',
        success: function(data){
            if(data == null) {
    
                $('#tabLog').html("<p class= 'error text-center'> Vous n'avez aucune notifications ! </p>");
                $('#tabHead').html("");
                $('#tabFoot').html("");

            } else {
                for (const [key, value] of Object.entries(data)) { 
                    tabH += 
                    "<th class='align-middle text-center'>"
                    + key + 
                    "</th>";
                }

                for(i = 0; i < Object.values(data)[0].length; i++) {

                    alert = '';
                    color = '';
                    space = '';

                    tab += "<tr>";


                    for (const [key, value] of Object.entries(data)) {

                        tab +=
                        "<td class='text-center'>" 
                        + value[i] +
                        "</td>";
                    }

                    tab += "</tr>";

                }

                for(i = 0; i < Object.values(data).length; i++) {

                    configuration.columns[i] = {orderable : true};

                }
                
                $('#tabLog').html(tab);
                $('#tabHead').html("<tr>" + tabH + "</tr>");
                $('#tabFoot').html("<tr>" + tabH + "</tr>");
                table = $('#playlist-table').DataTable(configuration);

            }
        }
    }).fail(function (jqXHR, textStatus, error) {
        console.log(error);
    });
})