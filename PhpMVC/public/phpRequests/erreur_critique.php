<?php

require 'manager.php';

$compteUtilisateur = '34000_EMITY34';
$bdd = Manager::ConnexionBDD();
$requete;
$requeteUp;
$player = [];
$j = 0;

$requete = $bdd->prepare("SELECT players__tasks.id_task, nom_player, players__tasks.id_player FROM players__tasks
JOIN players ON players__tasks.id_player = players.id_player
JOIN tasks ON players__tasks.id_task = tasks.id_task
JOIN type_notif__players ON type_notif__players.id_player = players.id_player
JOIN utilisateurs ON players.id_utilisateur = utilisateurs.id_utilisateur
WHERE compte_utilisateur = ?
AND type_notif__players.id_type_notif = 2
AND players__tasks.id_task = 2
AND bool_old_player = 0");

$requete->execute(array($compteUtilisateur));


while ($donneesPlayer = $requete->fetch()) {
    $player['nom_player'][$j] = $donneesPlayer['nom_player'];
    
    $requeteUp = $bdd->prepare("UPDATE players__tasks
    SET bool_old_player = 1
    WHERE id_player = ? AND id_task = 2");
    $requeteUp->execute(array($donneesPlayer['id_player']));

    $j++;
}

echo json_encode($player);

?>