<?php

require 'manager.php';

$compteUtilisateur = '34000_EMITY34';
$bdd = Manager::ConnexionBDD();
$message = [];
$statusPlayer;
$requeteInsert;
$requeteUp;
$i = 0;

$statusPlayer = file_get_contents('https://link.wepub.fr/webmanager/modeles/getSTATUS.php?compte=' . $compteUtilisateur . '');

$statusPlayer = json_decode($statusPlayer, true);


$requete = $bdd->prepare("SELECT HOUR(TIME(NOW())) AS Heure, players.id_player, id_task, compte_utilisateur, date_envoi_alerte_player, nom_player FROM players__tasks
JOIN players ON players__tasks.id_player = players.id_player
JOIN type_notif__players ON type_notif__players.id_player = players.id_player
JOIN utilisateurs ON players.id_utilisateur = utilisateurs.id_utilisateur
WHERE compte_utilisateur = ? 
AND TIMEDIFF(NOW(), date_envoi_alerte_player) > '01:00:00'
AND type_notif__players.id_type_notif = 2
AND bool_old_player = 0");

$requete->execute(array($compteUtilisateur));


while ($donneesPlayer = $requete->fetch()) {
    
    $heureDernierStatus = substr($statusPlayer[$donneesPlayer['nom_player']][0], 13, 2);

    if($donneesPlayer['id_task'] == 2 && (($donneesPlayer['Heure'] - $heureDernierStatus) >= 1 || ($donneesPlayer['Heure'] - $heureDernierStatus) < 0)) {
        
        $requeteUp = $bdd->prepare("UPDATE players__tasks
        SET bool_old_player = 1
        WHERE id_player = ? AND id_task = 2");
        $requeteUp->execute(array($donneesPlayer['id_player']));
        
        $requeteInsert = $bdd->prepare("INSERT INTO players__tasks (id_task, id_player, date_envoi_alerte_player, bool_old_player)
        VALUES (2, ?, NOW(), 0)");
        $requeteInsert->execute(array($donneesPlayer['id_player']));
        var_dump($donneesPlayer['id_player']);

        $i++;

    } else if($donneesPlayer['id_task'] == 3 && $statusPlayer[$donneesPlayer['nom_player']][2] == 1) {
        
        $requeteUp = $bdd->prepare("UPDATE players__tasks
        SET bool_old_player = 1
        WHERE id_player = ? AND id_task = 3");
        $requeteUp->execute(array($donneesPlayer['id_player']));
        
        $requeteInsert = $bdd->prepare("INSERT INTO players__tasks (id_task, id_player, date_envoi_alerte_player, bool_old_player)
        VALUES (3, ?, NOW(), 0, 0)");
        $requeteInsert->execute(array($donneesPlayer['id_player']));

        $i++;

    }
}

echo json_encode(array('nbrNotifs' => $i));





//$requete = $bdd->prepare("SELECT DATE(NOW()) AS dateNow, compte_utilisateur, date_envoi_alerte_utilisateur FROM utilisateurs__tasks
//JOIN utilisateurs ON utilisateurs__tasks.id_utilisateur = utilisateurs.id_utilisateur
//WHERE compte_utilisateur = ?
//AND TIME(date_envoi_alerte_utilisateur) < (TIME(NOW()) - INTERVAL 1 HOUR)");
//
//$requete->execute(array($compteUtilisateur));
//
//echo 'Utilisateur : </br>';
//while ($donneesUser = $requete->fetch()) {
//}

?>