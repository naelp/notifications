<?php

class Manager {
    
    static private $host = 'localhost';
    static private $login = 'emity_notif';
    static private $mdp = 'Panini34';
    static private $db = 'emity_notif';
    static private $bdd;

    public static function ConnexionBDD() {
    try {
        if(!isset(self::$bdd)) {
            self::$bdd = new PDO('mysql:host='.self::$host.';dbname='.self::$db, self::$login,self::$mdp);//acces BDD
        }
    } catch (Exception $e) {
        self::DeconnexionBDD();
        die('Erreur : ' . $e->getMessage());
    }
    return self::$bdd;
  }
  /** ------------------------------------------------------------------------
  * \date : 07/03/2019
  * \author : Maxime CHAMBAUD
  * \brief:   Fonction qui permet de se déconnecter de la BDD
  * \version 1.00
  * \note V1.00 creation 07/03/2019
  *-------------------------------------------------------------------------
  *-------------------------------------------------------------------------
  *-------------------------------------------------------------------------*/
  public static function DeconnexionBDD() {
    try {
        self::$bdd = null;
    } catch (Exception $e) {
        die('Erreur : ' . $e->getMessage());
    }
  }
}

?>