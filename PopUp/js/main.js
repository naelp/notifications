/**
 * This class have been create to add notifications to the interface EManager
 * The style will be customizable to match with the different users
 * 
 */

class Notif {



/**
 * Here are the attributes that will be used to change the alerts styles
 */

    constructor() {
        // Border color
        this.border;

        // Text color head
        this.colorHead;

        // Text color head
        this.colorBody;

        // Background-color button cancel
        this.buttonCancel;
        
        // Background-color button ok
        this.buttonOk;

        // Background-color header modal
        this.headerBack;

        // Message in the alert
        this.messageAlert;

        // Title of the modal
        this.titleModal;

        // Message in the modal
        this.messageModal;

        // Title of the modal with an input
        this.titleModalInput;

        // Message of the modal with an input
        this.messageModalInput;

        // Default value into the input
        this.defaultValueInput;

        // Title of the modal with confirmation
        this.titleModalChoice;

        // Message of the modal with confirmation
        this.messageModalChoice;

        // Background color of the alerts
        this.backColor;
    }
    
/** 
 * This method makes an alert modal appear with an input
 * In parameters we can add :
 * - The title of the alert
 * - The message of the alert to help the user
 * - The default value in the input
 * 
 * 
*/
    popNew() {
        alertify.set('notifier','delay', 3);
        alertify.prompt(this.titleModalInput, this.messageModalInput, this.defaultValueInput,
            function() { 
            }
            ,
// This function is called if the user click on 'cancel'
            function () {
                alertify.error('Cancel !')
            }

// The following happend when the user click on ok
// The function check if the input is empty or not
        )
        .set({'onok': function(e, value){
            if(!value) {
                e.cancel = true;
            } else {
                alertify.success('Valeur choisie : ' + value)
            }
        } })
        .set('defaultFocusOff', true)
        .set({ transition: 'zoom' }).show;

// Here are all the customization which will be different with all the users
        $('.ajs-dialog').css('border-color', this.border);
        $('.ajs-body').css('color', this.colorBody);
        $('.ajs-header').css('color', this.colorHead);
        $('.ajs-dialog, .ajs-body, .ajs-footer').css('background-color', this.backColor);
        $('.ajs-header').css('background-color', this.headerBack);
        $('.ajs-cancel').css('background-color', this.buttonCancel);
        $('.ajs-ok').css('background-color', this.buttonOk);
    }

/**
 * This method makes an alert modal appear with an information
 * In parameters we can add :
 * - The title of the alert
 * - The message of the alert to inform the user
 *
*/

    popInfos() {
        //The alert is set with a transition when it appears
        alertify.alert(this.titleModal, this.messageModal)
        .set('defaultFocusOff', true)
        .set({ transition: 'zoom' }).show;
        $('.ajs-dialog').css('border-color', this.border);
        $('.ajs-body').css('color', this.colorBody);
        $('.ajs-header').css('color', this.colorHead);
        $('.ajs-dialog, .ajs-body, .ajs-footer').css('background-color', this.backColor);
        $('.ajs-ok').css('background-color', this.buttonOk);
        $('.ajs-header').css('background-color', this.headerBack);
    }

/**
 * This method makes an notification appear in the bottom-right of the window
 * In parameters we can add :
 * - The message of the alert to inform the user
 *
*/

    popNotifs() {
        alertify.set('notifier','delay', 25);
        alertify.success(this.message);
        $('.ajs-dialog').css('border-color', this.border);
        $('.ajs-header').css('color', this.colorHead);
        $('.ajs-body').css('color', this.colorBody);
        $('.ajs-header').css('background-color', this.headerBack);
    }

/**
 * This method makes an alert modal appear and propose 2 choices to the user
 * In parameters we can add :
 * - The message of the alert that the user have to confirm
 *
*/
    popChoice() {
        alertify.confirm(this.messageModalChoice, 
            function(){ 
                $("#result").html('ok') 
            }, 
            function(){ 
                $("#result").html('cancel')
            }
        )
        .set({'labels': {ok: 'Oui', cancel: 'Non'}})
        .set({title: this.titleModalChoice})
        .set('defaultFocusOff', true)
        .set({ transition: 'zoom' }).show;
        $('.ajs-dialog').css('border-color', this.border);
        $('.ajs-dialog, .ajs-body, .ajs-footer').css('color', this.backColor);
        $('.ajs-body').css('color', this.colorBody);
        $('.ajs-header').css('color', this.colorHead);
        $('.ajs-header').css('background-color', this.headerBack);
        $('.ajs-dialog, .ajs-body, .ajs-footer').css('background-color', this.backColor);
        $('.ajs-ok').css('background-color', this.buttonOk);
        $('.ajs-cancel').css('background-color', this.buttonCancel);
    }

/**
 * All the setters we need to acces the attributes of the Notif class 
 */
    setBorder(border) {
        this.border = border;
    }

    setColorHead(colorHead) {
        this.colorHead = colorHead;
    }

    setColorBody(colorBody) {
        this.colorBody = colorBody;
    }

    setButtonCancel(buttonCancel) {
        this.buttonCancel = buttonCancel;
    }

    setButtonOk(buttonOk) {
        this.buttonOk = buttonOk;
    }

    setHeaderBack(headerBack) {
        this.headerBack = headerBack;
    }

    setMessage(message) {
        this.message = message;
    }

    setTitleModal(titleModal) {
        this.titleModal = titleModal;
    }

    setMessageModal(messageModal) {
        this.messageModal = messageModal;
    }

    setTitleModalInput(titleModalInput) {
        this.titleModalInput = titleModalInput;
    }

    setMessageModalInput(messageModalInput) {
        this.messageModalInput = messageModalInput;
    }

    setDefaultValueInput(defaultValueInput) {
        this.defaultValueInput = defaultValueInput;
    }

    setTitleModalChoice(titleModalChoice) {
        this.titleModalChoice = titleModalChoice;
    }

    setMessageModalChoice(messageModalChoice) {
        this.messageModalChoice = messageModalChoice;
    }

    setBackColor(backColor) {
        this.backColor = backColor;
    }

}

/**
 * Here is the instantiation of the object notif
 * This object will be used to customize all the parts of the alerts 
 */

var notif = new Notif();

notif.setBorder("var(--mainTextColorWithOpacity)");
notif.setColorHead("var(--mainTextColorActif)");
notif.setColorBody("var(--colorBtConnexionHover)");
notif.setButtonCancel("var(--textErreurColor)");
notif.setButtonOk("var(--textValideColor)");
notif.setHeaderBack("var(--colorBtConnexionHover)");
notif.setBackColor("var(--mainTextColorActif)");
notif.setMessage('Bienvenue !');
notif.setTitleModal('Notification');
notif.setMessageModal('Vous avez 1 notification(s) non lue(s)');
notif.setTitleModalInput('Choix temps décran');
notif.setMessageModalInput('Temps en secondes');
notif.setDefaultValueInput('15');
notif.setTitleModalChoice('Confirmez');
notif.setMessageModalChoice('Êtes-vous sûr ?');


/**
 * Test about the changes of text within the alert
 */
var i;

function icount(){

    i = Math.random();
    
    if(i > 0.5) {
        console.log(i);
        notif = new Notif();
    
        notif.setBorder("var(--mainTextColorWithOpacity)");
        notif.setColorHead("var(--mainTextColorActif)");
        notif.setColorBody("var(--colorBtConnexionHover)");
        notif.setButtonCancel("var(--textErreurColor)");
        notif.setButtonOk("var(--textValideColor)");
        notif.setHeaderBack("var(--colorBtConnexionHover)");
        notif.setBackColor("var(--mainTextColorActif)");
        notif.setMessage('Bienvenue !');
        notif.setTitleModal('Notification');
        notif.setMessageModal('Vous avez 1 notification(s) non lue(s)');
        notif.setTitleModalInput('Choix temps décran');
        notif.setMessageModalInput('Temps en secondes');
        notif.setDefaultValueInput('15');
        notif.setTitleModalChoice('Confirmez');
        notif.setMessageModalChoice('Êtes-vous sûr ?');
        notif.setMessage('Bienvenue !');
        notif.popNotifs();
    } else {
        console.log(i);
        notif = new Notif();
    
        notif.setBorder("var(--mainTextColorWithOpacity)");
        notif.setColorHead("var(--mainTextColorActif)");
        notif.setColorBody("var(--colorBtConnexionHover)");
        notif.setButtonCancel("var(--textErreurColor)");
        notif.setButtonOk("var(--textValideColor)");
        notif.setHeaderBack("var(--colorBtConnexionHover)");
        notif.setBackColor("var(--mainTextColorActif)");
        notif.setMessage('Bienvenue !');
        notif.setTitleModal('Notification');
        notif.setMessageModal('Vous avez 1 notification(s) non lue(s)');
        notif.setTitleModalInput('Choix temps décran');
        notif.setMessageModalInput('Temps en secondes');
        notif.setDefaultValueInput('15');
        notif.setTitleModalChoice('Confirmez');
        notif.setMessageModalChoice('Êtes-vous sûr ?');
        notif.setMessage('Au revoir !');
        notif.popNotifs();
    }
}

